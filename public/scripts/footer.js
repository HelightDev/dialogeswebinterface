
window.addEventListener("load", function () {
    relocateFooter();
    document.documentElement.addEventListener('DOMNodeInserted',function () {
        relocateFooter();
    });
});

function relocateFooter() {
    document.getElementById("bot-bar").style.top=Math.max(
        document.getElementById("wrapper").offsetHeight + 20, window.innerHeight
    )+"px";
}
